FROM maven:3.9.6-amazoncorretto-17

WORKDIR /build

COPY target/acc-system-0.0.1-SNAPSHOT.jar /build/acc-system-0.0.1-SNAPSHOT.jar

ENTRYPOINT ["java", "-jar", "acc-system-0.0.1-SNAPSHOT.jar"]
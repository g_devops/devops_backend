package com.practice.accsystem.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class ExcelGenerationException extends RuntimeException {
    public ExcelGenerationException(String message, Throwable cause) {
        super(message, cause);
    }
    public ExcelGenerationException(String message) {
        super(message);
    }
}

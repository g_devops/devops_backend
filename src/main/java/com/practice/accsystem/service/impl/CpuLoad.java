package com.practice.accsystem.service.impl;

import java.util.Random;

public class CpuLoad {
    public static void load() {
        int numThreads = Runtime.getRuntime().availableProcessors();
        for (int i = 0; i < numThreads; i++) {
            new Thread(new PiCalculator()).start();
        }
    }

    static class PiCalculator implements Runnable {
        @Override
        public void run() {
            long numPoints = 1_000_000_000L;
            Random random = new Random();
            long pointsInsideCircle = 0;

            for (long i = 0; i < numPoints; i++) {
                double x = random.nextDouble();
                double y = random.nextDouble();
                if (x * x + y * y <= 1.0) {
                    pointsInsideCircle++;
                }
            }

            double piEstimate = 4.0 * pointsInsideCircle / numPoints;
            System.out.println("Estimated Pi: " + piEstimate);
        }
    }
}

package com.practice.accsystem.entity;

public enum ContractType {
    PURCHASE,
    SUPPLY,
    WORKS
}
